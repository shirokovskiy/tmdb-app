# TheMovieDB App #

TheMovieDB Application to search movies, actors/actresses, etc.

### What is this application for? ###

* Looking for movies, actors and actresses, casts, reviews, etc.
* Version 1.0

### How do I get set up? ###

* Summary of set up

    Download App into your virtual host root directory.
    Be sure, downloaded files has proper permissions to run under web-server.
    
    For example:
    ```-rw-rw-r-- www-data www-data index.php```

* How to run

    Open your favorite web-browser, type in the URL field just after domain name the path to Application
    /tmdb-app/index.php
    
    i.e. [http://phpwebstudio.com/tmdb-app/index.php](http://phpwebstudio.com/tmdb-app/index.php)

* How to use

    Choose the type of search, by selecting "movies" or "actors/actresses"
    Type search query and click "Search" button.
    
    Depending on popularity of the query, search results will appears below the search form.
