/**
 * Created by Dmitry Sirokovskiy.
 * Email: info@phpwebstudio.com
 * Date: 17.07.18
 * Time: 15:28
 * Description:
 */

jQuery(document).ready(function ($) {
    $("#form-container").submit(function (event) {
        event.preventDefault();
        event.stopPropagation();

        if ($("#search-term").val() == "" || typeof $("#search-term").val() === "undefined") {
            // error
            $("#search-term").css({'border-color':'red','background-color':'#FFEEEE'});
            alert("Whoops, something missed..");
        } else {
            $("#result").empty().append(
                $('<h2>').text('Searching results...').css({'text-align':'center'})
            ).append(
                $('<img>').attr('src', 'images/ajax-loader-tr-2.gif').css({'width':'100%'})
            );

            // send request to search
            $.post(window.location.href, $("#form-container").serialize(), function (data) {
                if (data.state == "ok") {
                    $( "#result" ).empty().append( data.content );
                } else {
                    alert(data.message);
                }
            }, "json");
        }
    });
});
