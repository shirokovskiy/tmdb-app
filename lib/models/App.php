<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 17.07.18
 * Time         : 14:15
 * Description  :
 */

namespace App;

include_once "Tmdb.php";

class App
{
    protected $config;

    public function __construct($filename = 'app.json') {
        $this->config = json_decode( file_get_contents(DROOT.APPDIR."/config/".$filename ) );
    }

    public function run() {
        try {

            if(isset($_SERVER['HTTP_X_REQUESTED_WITH'])
                && !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
                && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
            {
                return $this->search();
            }

            $this->_toHtml();
        } catch (Exception $ex) {
            echo $ex->getTraceAsString();
        }
    }

    private function search() {
        # TMDB API query
        $json['state'] = 'ok';
        $json['message'] = '';
        $json['content'] = '';

        # todo: TheMovieDB API queries here
        $APIKey = $this->config->api_key;

        $tmdb = new \TMDB($APIKey, "en", false);

        $searchTerm = htmlentities( htmlspecialchars($_POST['search-term']) );
        $searchType = trim($_POST['search_type']);
        if (strlen($searchType)!=6 && !in_array($searchType,['movies','actors'])) {
            $searchType = 'movies';
        }

        switch ($searchType) {
            case "movies":
                $movies = $tmdb->searchMovie($searchTerm);
                break;

            case "actors":
                $actors = $tmdb->searchPerson($searchTerm);
                break;

            default: break;
        }


        ob_start();
        include_once DROOT.APPDIR."/templates/results.phtml";
        $json['content'] = ob_get_contents();
        ob_end_clean();

        echo json_encode($json);
    }

    protected function _toHtml() {
        require $this->config->template->dir . '/' . $this->config->template->file;
    }
}
