<?php
define ('APPDIR', basename(dirname(__FILE__)));

if (!isset($_SERVER['DOCUMENT_ROOT']) || empty($_SERVER['DOCUMENT_ROOT'])) {
    $_SERVER['DOCUMENT_ROOT'] = dirname(dirname(__FILE__));
}

if (!defined("DROOT")) {
    define('DROOT', $_SERVER['DOCUMENT_ROOT'].(preg_match("/.*\/$/", $_SERVER['DOCUMENT_ROOT']) ? "" : "/") );
}

require_once dirname(__FILE__)."/lib/models/App.php";

$app = new \App\App();

$app->run();
